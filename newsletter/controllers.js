const Router = require('express').Router();

const {
    notify
} = require('./services.js');

Router.post('/', (req, res) => {
    const {
        mailingList,
        book
    } = req.body;

    notify(mailingList, book); // nu am pus special await, pentru ca NU vreau sa astept dupa terminarea functiei

    res.status(204).end();
});

module.exports = Router;